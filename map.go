package mapper

import (
	"strings"
	"strconv"
)

type Value interface {
	Get(string) Value
	GetString(string) *string
	GetBool(string) *bool
	GetInt64(string) *int64
	GetArray(string) *[]Value
}

type Map map[string]Value

func (m *Map) Get(key string) Value {
	keys := Keys(strings.Split(key, "."))
	var v Value
	v = m
	for _, key := range keys {
		if nm, ok := v.(*Map); ok {
			if nv, ok := (*nm)[key]; ok {
				v = nv
			} else {
				return nil
			}
		} else {
			return nil
		}
	}
	return v
}

func (m *Map) GetString(key string) *string {
	if v := m.Get(key); v != nil {
		if k, ok := v.(*String); ok {
			s := string(*k)
			return &s
		}
	}
	return nil
}

func (m *Map) GetBool(key string) *bool {
	if v := m.Get(key); v != nil {
		if s, ok := v.(*String); ok {
			var b bool
			str := string(*s)
			if str == "true" || str == "1" {
				b = true
			}
			return &b
		}
	}
	return nil
}

func (m *Map) GetInt64(key string) *int64 {
	if v := m.Get(key); v != nil {
		if k, ok := v.(*String); ok {
			n, _ := strconv.ParseInt(string(*k), 10, 64)
			return &n
		}
	}
	return nil
}

func (m *Map) GetArray(key string) *[]Value {
	var arr []Value
	if a := m.Get(key); a != nil {
		if d, ok := a.(*Map); ok {
			for _, v := range *d {
				if v != nil {
					arr = append(arr, v)
				}
			}
		}
	}
	return &arr
}

func (m *Map) ExistKey(key string) bool {
	if _, ok := (*m)[key]; ok {
		return true
	}
	return false
}

func (m *Map) build(keys Keys, value string) {
	key, newKeys := keys.Shift()
	if newKeys.IsNil() {
		str := String(value)
		(*m)[key] = &str
	} else {
		if v, ok := (*m)[key]; ok && v != nil {
			if d, e := (*m)[key].(*Map); e {
				d.build(newKeys, value)
			}
		} else {
			nm := make(Map)
			nm.build(newKeys, value)
			(*m)[key] = &nm
		}
	}
}

/*
func (m *Map) Get(rawKey string) Value {
	keys := Keys(strings.Split(rawKey, "."))
	var v Value
	v = m
	for _, key := range keys {
		if nm, ok := v.(*Map); ok {
			if nv, ok := (*nm)[key]; ok {
				v = nv
			} else {
				return nil
			}
		} else {
			return nil
		}
	}
	return v
}
*/
