package mapper

import (
	"net/url"
	"strings"
)

func Parse(values url.Values) Map {
	return ParseCustom(values, defaultKeysHandler)
}

func ParseCustom(values url.Values, fn func(string) Keys) Map {
	m := make(Map)
	for rawKey, rawValue := range values {
		var value string
		if len(rawValue) > 0 {
			value = rawValue[0]
		}
		m.build(fn(rawKey), value)
	}
	return m
}

func defaultKeysHandler(rawKey string) (keys Keys) {
	return Keys(strings.Split(strings.ReplaceAll(rawKey, "]", ""), "["))
}
