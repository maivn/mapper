package mapper

type Keys []string

func (ks *Keys) Shift() (string, Keys) {
	if len(*ks) > 0 {
		return (*ks)[0], (*ks)[1:]
	}
	return "", []string{}
}

func (ks *Keys) IsNil() bool {
	if ks == nil || len(*ks) == 0 {
		return true
	}
	return false
}
