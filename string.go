package mapper

import "strconv"

type String string

func (s *String) Get(key string) Value {
	return nil
}

func (s *String) GetString(key string) *string {
	if key != "" {
		return nil
	}
	str := string(*s)
	return &str
}
func (s *String) GetBool(key string) *bool {
	if key != "" {
		return nil
	}
	var b bool
	str := string(*s)
	if str == "true" || str == "1" {
		b = true
	}
	return &b
}
func (s *String) GetInt64(key string) *int64 {
	if key != "" {
		return nil
	}
	n, _ := strconv.ParseInt(string(*s), 10, 64)
	return &n
}
func (s *String) GetArray(key string) *[]Value {
	return nil
}
